<?php

return [

    //-------------------------------
    // Timezone for insert dates in database
    // If you want Gateway not set timezone, just leave it empty
    //--------------------------------
    'timezone' => 'Asia/Tehran',

    //--------------------------------
    // PayIr gateway
    //--------------------------------
    'vandar'    => [
        'api_key'          => 'xxxxxxxxxxxxxxxxxxxx',
        'callback_url' => '/'
    ],

    //-------------------------------
    // Tables names
    //--------------------------------
    'table'    => 'gateway_transactions',
];
