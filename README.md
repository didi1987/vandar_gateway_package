



package's home : https://vandar.io

Vandar IPG gateway, Connect your laravel project to vandar ipg api

( This Package is now compatible with both **4.\* and 5.\* versions of Laravel** ).


**Installation**:

Run below statements on your terminal :

STEP 1 : 

    composer require vandar/gateway
    
STEP 2 : Add `provider` and `facade` in config/app.php

    'providers' => [
      ...
      Vandar\Gateway\GatewayServiceProvider::class, // <-- add this line at the end of provider array
    ],


    'aliases' => [
      ...
      'Gateway' => Vandar\Gateway\Gateway::class, // <-- add this line at the end of aliases array
    ]

Step 3:  

    php artisan vendor:publish --provider=Vandar\Gateway\GatewayServiceProvider

Step 4: 

    php artisan migrate


Configuration file is placed in config/gateway.php , open it and enter your banks credential:

You can make connection to bank by several way (Facade , Service container):

    try {
       
       $gateway = \Gateway::make(new \Vandar());

       // $gateway->setCallback(url('/path/to/callback/route')); You can also change the callback
       $gateway
            ->price(1000)
            // setShipmentPrice(10) // optional - just for paypal
            // setProductName("My Product") // optional - just for paypal
            ->ready();

       $refId =  $gateway->refId(); // شماره ارجاع بانک
       $transID = $gateway->transactionId(); // شماره تراکنش

      // در اینجا
      //  شماره تراکنش  بانک را با توجه به نوع ساختار دیتابیس تان 
      //  در جداول مورد نیاز و بسته به نیاز سیستم تان
      // ذخیره کنید .
      
       return $gateway->redirect();
       
    } catch (\Exception $e) {
       
       	echo $e->getMessage();
    }

you can call the gateway in these ways :
 1. Gateway::make(new Vandar());
 1. Gateway::Vandar()
 2. app('gateway')->make(new Vandar());
 3. app('gateway')->Vandar();


In `price` method you should enter the price in IRR (RIAL) 

and in your callback :

    try { 
       
       $gateway = \Gateway::verify();
       $trackingCode = $gateway->trackingCode();
       $refId = $gateway->refId();
       $cardNumber = $gateway->cardNumber();
       
        // تراکنش با موفقیت سمت بانک تایید گردید
        // در این مرحله عملیات خرید کاربر را تکمیل میکنیم
    
    } catch (\Vandar\Gateway\Exceptions\RetryException $e) {
    
        // تراکنش قبلا سمت بانک تاییده شده است و
        // کاربر احتمالا صفحه را مجددا رفرش کرده است
        // لذا تنها فاکتور خرید قبل را مجدد به کاربر نمایش میدهیم
        
        echo $e->getMessage() . "<br>";
        
    } catch (\Exception $e) {
       
        // نمایش خطای بانک
        echo $e->getMessage();
    }  

If you are intrested to develop this package you can help us in these ways :

 1. Improving documents.
 2. Reporting issue or bugs.

This package is extended from PoolPort  but we've changed some functionality and improved it .
