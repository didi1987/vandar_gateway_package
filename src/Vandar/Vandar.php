<?php
namespace Vandar\Gateway\Vandar;

use Illuminate\Support\Facades\Input;
use Vandar\Gateway\Enum;
use Vandar\Gateway\PortAbstract;
use Vandar\Gateway\PortInterface;

class Vandar extends PortAbstract implements PortInterface
{
    /**
     * Address of main CURL server
     *
     * @var string
     */
    protected $serverUrl = 'https://vandario.ir/api/ipg/send';

    /**
     * Address of CURL server for verify payment
     *
     * @var string
     */
    protected $serverVerifyUrl = 'https://vandario.ir/api/ipg/verify';
    /**
     * Address of gate for redirect
     *
     * @var string
     */
    protected $gateUrl = 'https://vandario.ir/ipg/';


    protected $factorNumber;

    /**
     * {@inheritdoc}
     */
    public function set($amount)
    {
        $this->amount = $amount * 10;
        return $this;
    }

    /**
     * تعیین شماره فاکتور (اختیاری)
     *
     * @param $factorNumber
     *
     * @return $this
     */
    public function setFactorNumber($factorNumber)
    {
        $this->factorNumber = $factorNumber;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function ready()
    {
        $this->sendPayRequest();
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function redirect()
    {
        return redirect()->to($this->gateUrl . $this->refId);
    }

    /**
     * {@inheritdoc}
     */
    public function verify($transaction)
    {
        parent::verify($transaction);
        $this->userPayment();
        $this->verifyPayment();
        return $this;
    }

    /**
     * Sets callback url
     *
     * @param $url
     */
    function setCallback($url)
    {
        $this->callbackUrl = $url;
        return $this;
    }

    /**
     * Gets callback url
     * @return string
     */
    function getCallback()
    {
        if (!$this->callbackUrl)
            $this->callbackUrl = $this->config->get('gateway.Vandar.callback-url');
        return urlencode($this->makeCallback($this->callbackUrl, ['transaction_id' => $this->transactionId()]));
    }

    /**
     * Send pay request to server
     *
     * @return void
     *
     * @throws VandarSendException
     */
    protected function sendPayRequest()
    {
        $this->newTransaction();
        $fields = [
            'api'      => $this->config->get('gateway.Vandar.api'),
            'amount'   => $this->amount,
            'redirect' => $this->getCallback(),
        ];

        if (isset($this->factorNumber))
            $fields['factorNumber'] = $this->factorNumber;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->serverUrl);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $response = json_decode($response, true);
        curl_close($ch);
        if (is_numeric($response['status']) && $response['status'] > 0) {
            $this->refId = $response['transId'];
            $this->transactionSetRefId();
            return true;
        }
        $this->transactionFailed();
        $this->newLog($response['errorCode'], VandarSendException::$errors[ $response['errorCode'] ]);
        throw new VandarSendException($response['errorCode']);
    }

    /**
     * Check user payment with GET data
     *
     * @return bool
     *
     * @throws VandarReceiveException
     */
    protected function userPayment()
    {
        $status = Input::get('status');
        $transId = Input::get('transId');
        $this->cardNumber = Input::get('cardNumber');
        $message = Input::get('message');
        if (is_numeric($status) && $status > 0) {
            $this->trackingCode = $transId;
            return true;
        }
        $this->transactionFailed();
        $this->newLog(-5, $message);
        throw new VandarReceiveException(-5);
    }

    /**
     * Verify user payment from zarinpal server
     *
     * @return bool
     *
     * @throws VandarReceiveException
     */
    protected function verifyPayment()
    {
        $fields = [
            'api'     => $this->config->get('gateway.Vandar.api'),
            'transId' => $this->refId(),
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->serverVerifyUrl);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $response = json_decode($response, true);
        curl_close($ch);
        if ($response['status'] == 1) {
            $this->transactionSucceed();
            $this->newLog(1, Enum::TRANSACTION_SUCCEED_TEXT);
            return true;
        }

        $this->transactionFailed();
        $this->newLog($response['errorCode'], VandarReceiveException::$errors[ $response['errorCode'] ]);
        throw new VandarReceiveException($response['errorCode']);
    }
}
